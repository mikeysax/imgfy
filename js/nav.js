window.addEventListener('DOMContentLoaded', function() {
  renderNav();
});

function renderNav() {
  var navbarContainer = document.createElement('div');
  navbarContainer.className = 'navbar-container';

  var logo = document.createElement('h1');
  logo.className = 'logo';
  logo.textContent = 'imgfy';

  logo.addEventListener('click', function() {
    window.location.href = 'index.html';
  });

  navbarContainer.appendChild(logo);
  navbarContainer.appendChild(renderSearch());

  var body = document.getElementsByTagName('body')[0];
  body.prepend(navbarContainer);
}

function renderSearch() {
  var searchContainer = document.createElement('div');
  searchContainer.className = 'search-container';

  var searchLabel = document.createElement('span');
  searchLabel.textContent = 'Search';

  var form = document.createElement('form');
  var input = document.createElement('input');
  input.type = 'text';

  form.addEventListener('submit', function(e) {
    e.preventDefault();
    var queryString = encodeURI(input.value);
    window.location.href = 'index.html?search=' + queryString;
  });

  form.appendChild(input);
  searchContainer.appendChild(searchLabel);
  searchContainer.appendChild(form);

  return searchContainer;
}

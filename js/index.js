window.addEventListener('DOMContentLoaded', function() {
  getImages();
});

function getImages() {
  var apiBaseURL = 'https://imgfy.herokuapp.com/';
  // var search = new URLSearchParams(window.location.search).getAll('search')[0];
  //
  // var finalURL = search ? apiBaseURL + '?search=' + encodeURI(search) : apiBaseURL;

  fetch(apiBaseURL)
  .then(function(response) {
    return response.json();
  })
  .then(function(json) {
    var images = json.hits;
    return renderImageList(images);
  })
  .catch(function(error) {
    console.log('Error:', error);
  });
}

function renderImageList(images) {
  var imageListLocation = document.getElementById('app');

  for (var i = 0; i < images.length; i++) {
    imageListLocation.appendChild(renderImage(images[i]));
  }
}

function renderImage(image) {
  var imageContainer = document.createElement('a');
  imageContainer.className = 'image-container';
  imageContainer.href = 'show.html?id=' + image.id;

  var img = document.createElement('img');
  img.src = image.webformatURL;

  imageContainer.appendChild(img);
  imageContainer.appendChild(renderUser(image.user, image.userImageURL));

  return imageContainer;
}

function renderUser(userName, userImage) {
  var userContainer = document.createElement('span');
  userContainer.className = 'user';

  var name = document.createElement('p');
  name.innerHTML = userName;

  var avatar = document.createElement('img');
  avatar.src = userImage;

  userContainer.appendChild(name);
  userContainer.appendChild(avatar);

  return userContainer;
}

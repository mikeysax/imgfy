window.addEventListener('DOMContentLoaded', function() {
  getPicture();
});

function getPicture() {
  var id = new URLSearchParams(window.location.search).getAll('id')[0];

  var apiBaseUrl = 'https://imgfy.herokuapp.com/pictures/' + id;

  fetch(apiBaseUrl)
  .then(function(response) {
    return response.json();
  })
  .then(function(json) {
    return renderPage(json);
  })
  .catch(function(error) {
    console.log('Error:', error);
  });
}

function renderPage(data) {
  var image = data.hits[0];
  var comments = data.comments;

  var app = document.getElementById('app');

  app.appendChild(renderImage(image));
  app.appendChild(renderCommentForm(image.id));
  app.appendChild(renderComments(comments));
}

function renderImage(image) {
  var imageContainer = document.createElement('div');
  imageContainer.className = 'image-container';

  var img = document.createElement('img');
  img.className = 'image';
  img.src = image.webformatURL;

  imageContainer.appendChild(renderUser(image.user, image.userImageURL));
  imageContainer.appendChild(img);
  imageContainer.appendChild(renderTags(image.tags));

  return imageContainer;
}

function renderUser(authorName, authorImage) {
  var user = document.createElement('span');
  user.className = 'user';

  var name = document.createElement('p');
  name.innerHTML = authorName;

  var avatar = document.createElement('img');
  avatar.src = authorImage;

  user.appendChild(name);
  user.appendChild(avatar);

  return user
}

function renderTags(tags) {
  var tagArray = tags.split(', ')

  var tagContainer = document.createElement('div');
  tagContainer.className = 'tag-container';

  tagArray.map(function(tag) {
    var tagElement = document.createElement('a');
    tagElement.className = 'tag';
    tagElement.textContent = tag;
    tagElement.href = 'index.html?search=' + tag;

    tagContainer.appendChild(tagElement);
  });

  return tagContainer;
}

function renderCommentForm(pictureId) {
  var formContainer = document.createElement('div');
  formContainer.className = 'form-container';

  var formTitle = document.createElement('h2');
  formTitle.textContent = 'New Comment';

  var form = document.createElement('form');
  var nameInput = document.createElement('input');
  nameInput.type = 'text';
  nameInput.placeholder = 'Name...';
  nameInput.required = true;

  var messageInput = document.createElement('textarea');
  messageInput.type = 'text';
  messageInput.placeholder = 'Message...';
  messageInput.required = true;

  var submitButton = document.createElement('button');
  submitButton.type = 'submit';
  submitButton.textContent = 'Submit';

  form.appendChild(nameInput);
  form.appendChild(messageInput);
  form.appendChild(submitButton);

  form.addEventListener('submit', function(e) {
    e.preventDefault();

    var commentParams = {
      comment: {
        name: nameInput.value,
        message: messageInput.value,
        picture_id: pictureId
      }
    };

    fetch('https://imgfy.herokuapp.com/comments', {
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      method: 'POST',
      body: JSON.stringify(commentParams)
    })
    .then(function(response) {
      return response.json();
    })
    .then(function(json) {
      form.reset();

      var commentsContainer = document.getElementsByClassName('comments-container')[0];
      var comment = createComment(json);
      commentsContainer.prepend(comment);
    })
    .catch(function(error) {
      console.log('Error:', error)
    });
  });

  formContainer.appendChild(formTitle);
  formContainer.appendChild(form);

  return formContainer;
}

function renderComments(commentArray) {
  var commentsContainer = document.createElement('div');
  commentsContainer.className = 'comments-container';

  commentArray.map(function(data) {
    var comment = createComment(data);
    commentsContainer.prepend(comment);
  });

  return commentsContainer;
}

function createComment(data) {
  var comment = document.createElement('div');
  comment.className = 'comment';
  comment.id = 'comment-' + data.id;

  var name = document.createElement('span');
  name.className = 'name';
  name.textContent = data.name;

  var message = document.createElement('span');
  message.className = 'message';
  message.textContent = data.message

  var bottomRow = document.createElement('div');
  bottomRow.className = 'bottom-row';

  bottomRow.appendChild(renderEditButton(data));
  bottomRow.appendChild(renderDeleteButton(data));

  comment.appendChild(name);
  comment.appendChild(message);
  comment.appendChild(bottomRow);

  return comment;
}

function renderDeleteButton(data) {
  var deleteButton = document.createElement('small');
  deleteButton.className = 'delete';
  deleteButton.textContent = 'Delete';

  deleteButton.addEventListener('click', function(e) {
    fetch('https://imgfy.herokuapp.com/comments/' + data.id, {
      method: 'DELETE'
    })
    .then(function(response) {
      return document.getElementById('comment-' + data.id).remove();
    })
    .catch(function(error) { console.log('Error:', error); });
  });

  return deleteButton;
}

function renderEditButton(data) {
  var editButton = document.createElement('small');
  editButton.className = 'edit';
  editButton.textContent = 'Edit';

  editButton.addEventListener('click', function(e) {
    showEditForm(data);
  });

  return editButton;
}

function showEditForm(data) {
  var comment = document.getElementById('comment-' + data.id);
  for (var i = 0; i < comment.children.length; i++ ) {
    comment.children[i].style.display = 'none';
  }

  comment.appendChild(renderCommentEditForm(data));
}

function renderCommentEditForm(data) {
  var editForm = document.createElement('form');
  editForm.className = 'edit-form';

  var nameInput = document.createElement('input');
  nameInput.type = 'text';
  nameInput.required = true;
  nameInput.value = data.name;

  var messageInput = document.createElement('textarea');
  messageInput.type = 'text';
  messageInput.required = true;
  messageInput.value = data.message;

  var submitButton = document.createElement('button');
  submitButton.type = 'submit';
  submitButton.textContent = 'Update';

  editForm.addEventListener('submit', function(e) {
    e.preventDefault();

    var updatedCommentParams = {
      comment: {
        name: nameInput.value,
        message: messageInput.value
      }
    };

    fetch('https://imgfy.herokuapp.com/comments/' + data.id, {
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      method: 'PATCH',
      body: JSON.stringify(updatedCommentParams)
    })
    .then(function(response) {
      return response.json();
    })
    .then(function(json) {
      updateComment(json);
    })
    .catch(function(error) { console.log('Error:', error); });
  });

  editForm.appendChild(nameInput);
  editForm.appendChild(messageInput);
  editForm.appendChild(submitButton);

  return editForm;
}

function updateComment(data) {
  var comment = document.getElementById('comment-' + data.id);

  comment.children[0].textContent = data.name;
  comment.children[1].textContent = data.message;

  for (var i = 0; i < comment.children.length; i++ ) {
    comment.children[i].style.display = null;
  }

  comment.removeChild(comment.children[3]);
}
